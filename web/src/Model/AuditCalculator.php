<?php

namespace App\Model;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Shop;
Use App\Entity\ShopsToControll;
Use App\Entity\DoneAudits;
Use App\Entity\ControlledShops;
use App\Repository\ShopRepository;
use App\Repository\ShopsToControllRepository;
use App\Repository\DoneAuditsRepository;
use App\Repository\ControlledShopsRepository;

Class AuditCalculator
{
    private ShopRepository $shopRepository;
    private ShopsToControllRepository $shopsToControllRepository;
    private DoneAuditsRepository $doneAuditsRepository;
    private ControlledShopsRepository $controlledShopsRepository;

    public function __construct(
        ShopRepository $shopRepository,
        ShopsToControllRepository $shopsToControllRepository,
        DoneAuditsRepository $doneAuditsRepository,
        ControlledShopsRepository $controlledShopsRepository
    ) {
        $this->shopRepository = $shopRepository;
        $this->shopsToControllRepository = $shopsToControllRepository;
        $this->doneAuditsRepository = $doneAuditsRepository;
        $this->controlledShopsRepository = $controlledShopsRepository;
    }

    public function calculateRemainingShops(string $regionalCompany): array
    {
        $shopsToControllSum = $this->shopsToControllRepository->findByRegionalCompany($regionalCompany);
        $remaining['1'] = ($shopsToControllSum[0]->getCategoryA() - $this->calculateAuditsByCategory($regionalCompany, 1));
        $remaining['2'] = ($shopsToControllSum[0]->getCategoryB() - $this->calculateAuditsByCategory($regionalCompany, 2));
        $remaining['3'] = ($shopsToControllSum[0]->getCategoryC() - $this->calculateAuditsByCategory($regionalCompany, 3));
        $remaining['4'] = ($shopsToControllSum[0]->getCategoryD() - $this->calculateAuditsByCategory($regionalCompany, 4));
        $remaining['5'] = ($shopsToControllSum[0]->getCategoryE() - $this->calculateAuditsByCategory($regionalCompany, 5));
        $remainingShopsSum = $remaining;

        return $remainingShopsSum;
    }

    public function checkDuplicatedShop(string $regionalCompany): array
    {   
        $temporaryArray = [];
        $duplications = [];
        foreach ($this->doneAuditsRepository->findDoneAuditsByRegionalCompany($regionalCompany) as $shop) {
            if (in_array($shop->getShop(), $temporaryArray)) {
                $duplications[] = $shop;
            }
            $temporaryArray[] = $shop->getShop();
        }
    
        return $duplications;
    }

    public function showRandomRemainigShopsToAuditByCategory(string $regionalCompany, string $category): array
    {    
        $numberOfShops = $this->calculateRemainingShops($regionalCompany);
        $shops = $this->shopRepository->findRemainigShopsByCategory($regionalCompany, $category);  
        shuffle($shops);
        $randomShops = [];
        for ($i = 0; $i <= ($numberOfShops[$category]-1); $i++) {
            $randomShops[] = $shops[$i];
        }
        if (!is_null($randomShops)) {

        return $randomShops;
        }
    }
    
    private function calculateAuditsByCategory(string $regionalCompany, int $category): int
    {
        $sumOfAuditsBycategory = count($this->doneAuditsRepository->findDoneAuditsByRegionalCompanyAndCategory($regionalCompany, $category));
        
        return $sumOfAuditsBycategory;
    }
}