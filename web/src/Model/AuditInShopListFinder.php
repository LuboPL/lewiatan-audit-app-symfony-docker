<?php
namespace App\Model;

use App\Service\StringAdapter;
use App\Repository\ShopRepository;
use App\Entity\Shop;
use DateTime;

use Doctrine\Persistence\ManagerRegistry;

class AuditInShopListFinder 

{
    private StringAdapter $stringAdapter;
    private ShopRepository $shopRepository;

    public function __construct(
        StringAdapter $stringAdapter,
        ShopRepository $shopRepository
    ) {
        $this->stringAdapter = $stringAdapter;
        $this->shopRepository = $shopRepository;
    }

    public function prepareAuditsToInsert(array $doneAuditsData, DateTime $datetime): array
    {
        $doneAuditsInsertData = [];
        foreach ($this->shopRepository->fetchAllShops() as $shop) {
            if ($this->findAuditsInShopList($shop->getAdress(), $shop->getCity(), $shop, $doneAuditsData, $datetime)) {
                $doneAudit = $this->findAuditsInShopList($shop->getAdress(), $shop->getCity(), $shop, $doneAuditsData, $datetime);
                $doneAuditsInsertData[] = $doneAudit;
            }
        }

        return $doneAuditsInsertData;    
    }

    public function findAuditsInShopList(
        string $shopAdress,
        string $city,
        Shop $shop,
        array $doneAuditsData,
        DateTime $datetime
    ): array {   
        $audits = [];
        foreach ($doneAuditsData as $audit) {
            if (str_contains($this->stringAdapter->cutAdressString($shopAdress), $this->stringAdapter->cutAdressString($audit['adress'])) &&
                (str_contains($this->stringAdapter->cutCityString($city), $this->stringAdapter->cutCityString($audit['city']))) && 
                $datetime->format('Y-m-d') < $audit['date']) {             
                $auditData['id'] = $shop;
                $auditData['date'] = $audit['date'];
                $auditData['salesRepresentative'] = $audit['salesRepresentative'];
                $auditData['signer'] = $audit['signer'];
                $auditData['shouldBeSku'] = $audit['shouldBeSku'];
                $auditData['currentSku'] = $audit['currentSku'];
                $audits[] = $auditData;
            }
        }
   
        return $audits;
    }
}