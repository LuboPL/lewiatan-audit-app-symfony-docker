<?php

namespace App\Service;

class StringAdapter
{
    public function cutAdressString(string $adress): string
    {
        if (str_contains(mb_strtolower($adress,"UTF-8"), 'ul.') ||  
            str_contains(mb_strtolower($adress,"UTF-8"), 'al.') || 
            str_contains(mb_strtolower($adress,"UTF-8"), 'os.') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'ul. ') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'al. ') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'os. ') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'ul.  ') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'al.  ') ||
            str_contains(mb_strtolower($adress,"UTF-8"), 'os.  ')) {
            return mb_strtolower(substr($adress, 3, 9), 'UTF-8');
        } else {
            return mb_strtolower(substr($adress, 0, 9), 'UTF-8');
        }
    }

    public function cutCityString(string $city): string
    {
        mb_strtolower($city,"UTF-8");
        return mb_strtolower(substr($city, 0, 9), 'UTF-8');
    }
}
