<?php

namespace App\Service;

class UriMapper
{
    public function changePathToDbQueryString(string $regionalCompany): string
    {
        $regionalCompanyMap = [
            'malopolska' => 'Małopolska',
            'zory' => 'Żory',
            'slask' => 'Śląsk',
            'bielsko-biala' => 'Bielsko-Biała',
            'bjanex' => 'Bjanex',
            'kujawy' => 'Kujawy',
            'mazowsze' => 'Mazowsze',
            'opole' => 'Opole',
            'orbita' => 'Orbita',
            'podkarpacie' => 'Podkarpacie',
            'podlasie' => 'Podlasie',
            'polnoc' => 'Północ',
            'razem' => 'Razem',
            'wielkopolska' => 'Wielkopolska',
            'zachod' => 'Zachód'
        ];
        return str_replace(array_keys($regionalCompanyMap), $regionalCompanyMap, $regionalCompany);
    }

    public function changeCategoryNameToId(string $category): string
    {
        $categoryMap = [
            'a' => '1',
            'b' => '2',
            'c' => '3',
            'd' => '4',
            'e' => '5',
            'f' => '6'
        ];
        return str_replace(array_keys($categoryMap), $categoryMap, $category);
    }
}
