<?php

namespace App\Repository;

use App\Entity\ShopsToControll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShopsToControll|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopsToControll|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopsToControll[]    findAll()
 * @method ShopsToControll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopsToControllRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShopsToControll::class);
    }

    public function findByRegionalCompany($value): array
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.regional_company = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }
}
