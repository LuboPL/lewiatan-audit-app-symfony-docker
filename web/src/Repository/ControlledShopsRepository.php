<?php

namespace App\Repository;

use App\Entity\ControlledShops;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ControlledShops|null find($id, $lockMode = null, $lockVersion = null)
 * @method ControlledShops|null findOneBy(array $criteria, array $orderBy = null)
 * @method ControlledShops[]    findAll()
 * @method ControlledShops[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ControlledShopsRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ControlledShops::class);
    }

    public function fetchSumOfAudits()
    {
        return $this->createQueryBuilder('c')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    public function updateControlledShopsSum($doctrine, $filename): void
    {
        $entityManager = $doctrine->getManager();
        $this->prepareToUpdateControlledShopsSum($doctrine);
        foreach ($this->xlsxAdapter->getControlledShopsSum($filename) as $category) {       
            $controlledShops = new ControlledShops();
            $controlledShops->setRegionalCompany($category['regionalCompany']);
            $controlledShops->setCategoryA($category['a']);
            $controlledShops->setCategoryB($category['b']);
            $controlledShops->setCategoryC($category['c']);
            $controlledShops->setCategoryD($category['d']);
            $controlledShops->setCategoryE($category['e']);
            $controlledShops->setCategoryF($category['f']);
            $entityManager->persist($controlledShops);
            $entityManager->flush();
        }
    }

    public function findByRegionalCompany($value): array
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.regionalCompany = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }

    private function prepareToUpdateControlledShopsSum(ManagerRegistry $doctrine): void
    {
        $entityManager = $doctrine->getManager();
        $controlledShopsSum = $this->fetchSumOfAudits();
        foreach ($controlledShopsSum = $this->fetchSumOfAudits() as $category) {
            if (!is_null($category)) {
                $entityManager->remove($category);
                $entityManager->flush();
            }
        }
    }
}
