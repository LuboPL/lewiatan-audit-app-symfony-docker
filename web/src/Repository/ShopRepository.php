<?php

namespace App\Repository;

use App\Entity\Shop;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    public function findByRegionalCompany($regionalCompany): array
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.regional_company = :regionalCompany')
            ->setParameter('regionalCompany', $regionalCompany)
            ->orderBy('s.city', 'ASC')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findRemainigShopsByRegionalCompany($regionalCompany): array
    {
        return $this->createQueryBuilder('s')
            ->leftjoin('s.shop', 'd')
            ->where('d.shop is null')
            ->andWhere('s.regional_company = :regionalCompany')
            ->setParameter('regionalCompany', $regionalCompany)
            ->orderBy('s.city', 'ASC')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findRemainigShopsByCategory($regionalCompany, $category): array
    {
        return $this->createQueryBuilder('s')
            ->leftjoin('s.shop', 'd')
            ->where('d.shop is null')
            ->andWhere('s.regional_company = :regionalCompany')
            ->andWhere('s.category = :category')
            ->setParameter('regionalCompany', $regionalCompany)
            ->setParameter('category', $category)
            ->orderBy('s.city', 'ASC')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }

    public function fetchAllShops(): array
    {
        return $this->createQueryBuilder('s')
            ->orderBy('s.city', 'ASC')
            ->setMaxResults(5000)
            ->getQuery()
            ->getResult()
        ;
    }

    public function insertShop(
        string $regional_company,
        string $short_name,
        string $name,
        int $nip,
        string $postcode,
        string $city,
        string $adress,
        Category $category,
        string $accession_date,
        ManagerRegistry $doctrine
    ): void {   
        $entityManager = $doctrine->getManager();
        $shop = new Shop(
            $regional_company,
            $short_name,
            $name,
            $nip,
            $postcode,
            $city,
            $adress,
            $category,
            $accession_date
        );
        $entityManager->persist($shop);
        $entityManager->flush();
    }
}
