<?php

namespace App\Repository;

use App\Entity\DoneAudits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Shop;
use DateTime;

/**
 * @method DoneAudits|null find($id, $lockMode = null, $lockVersion = null)
 * @method DoneAudits|null findOneBy(array $criteria, array $orderBy = null)
 * @method DoneAudits[]    findAll()
 * @method DoneAudits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoneAuditsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoneAudits::class);
    }

    public function fetchAllAudits(): array
    {
        return $this->createQueryBuilder('d')
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }

    public function insertDoneAudit(
        string $date,
        string $salesRepresentative,
        string $signer,
        int $shouldBeSku,
        ?int $currentSku,
        Shop $shop,
        ManagerRegistry $doctrine
    ): void {   
        $entityManager = $doctrine->getManager();
        $doneAudit = new DoneAudits(
            $date,
            $salesRepresentative,
            $signer,
            $shouldBeSku,
            $currentSku,
            $shop
        );
        $entityManager->persist($doneAudit);
        $entityManager->flush();
    }

    public function findDoneAuditsByRegionalCompany($regionalCompany): array
    {
        return $this->createQueryBuilder('s')
            ->leftjoin('s.shop', 'd')
            ->setParameter('regionalCompany', $regionalCompany)
            ->where('d.regional_company = :regionalCompany')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findDoneAuditsByRegionalCompanyAndCategory($regionalCompany, $category): array
    {
        return $this->createQueryBuilder('s')
            ->leftjoin('s.shop', 'd')
            ->select('DISTINCT d.id')
            ->setParameter('regionalCompany', $regionalCompany)
            ->setParameter('category', $category)
            ->where('d.regional_company = :regionalCompany')
            ->andWhere('d.category = :category')
            ->setMaxResults(500)
            ->getQuery()
            ->getResult()
        ;
    }
}
