<?php
namespace App\Controller;

use App\Form\DoneAuditsFileType;
use App\Form\ShopListFileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use App\Service\FileUploader;
use App\Repository\DoneAuditsRepository;
use App\Repository\ShopRepository;
use App\Repository\ControlledShopsRepository;
use App\Repository\CategoryRepository;
use App\Xlsx\XlsxFileLoader;
use App\Xlsx\DoneAuditsDataPreperer;
use App\Xlsx\ShopListPreperer;
use DateTime;
use App\Entity\Shop;
use App\Entity\Category;
use App\Entity\ControlledShops;
use App\Model\AuditInShopListFinder;
use App\Model\AuditCalculator;
use Doctrine\Persistence\ManagerRegistry;

class UploadController extends AbstractController

{
    private XlsxFileLoader $xlsxFileLoader;
    private DoneAuditsDataPreperer $doneAuditsDataPreperer;
    private ShopListPreperer $shopListPreperer;
    private ShopRepository $shopRepository;
    private AuditInShopListFinder $auditInShopListFinder;
    private DoneAuditsRepository $doneAuditsRepository;
    private ControlledShopsRepository $controlledShopsRepository;
    private AuditCalculator $auditCalculator;
    private CategoryRepository $categoryRepository;
 
    public function __construct(
        XlsxFileLoader $xlsxFileLoader,
        DoneAuditsDataPreperer $doneAuditsDataPreperer,
        ShopListPreperer $shopListPreperer,
        ShopRepository $shopRepository,
        AuditInShopListFinder $auditInShopListFinder,
        DoneAuditsRepository $doneAuditsRepository,
        ControlledShopsRepository $controlledShopsRepository,
        AuditCalculator $auditCalculator,
        CategoryRepository $categoryRepository
    ) {
        $this->xlsxFileLoader = $xlsxFileLoader;
        $this->doneAuditsDataPreperer = $doneAuditsDataPreperer;
        $this->shopListPreperer = $shopListPreperer;
        $this->shopRepository = $shopRepository;
        $this->auditInShopListFinder = $auditInShopListFinder;
        $this->doneAuditsRepository = $doneAuditsRepository;
        $this->controlledShopsRepository = $controlledShopsRepository;
        $this->auditCalculator = $auditCalculator;
        $this->categoryRepository = $categoryRepository;
    }
    /**
     * @Route("/upload/doneaudits", name="upload_done_audits")
     */
    public function uploadDoneAudits(Request $request, FileUploader $fileUploader, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(DoneAuditsFileType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $doneAuditsFile */
            $doneAuditsFile = $form->get('doneAudits')->getData();
            if ($doneAuditsFile) {
                $filename = $fileUploader->upload($doneAuditsFile);
            }
        }
        if (isset($filename)) {
            $datetime = new DateTime('-77 days');
            $spreadsheet = $this->xlsxFileLoader->loadFromFile($filename);
            $doneAuditsData = $this->doneAuditsDataPreperer->getDoneAuditsData($spreadsheet);
            
            foreach ($this->auditInShopListFinder->prepareAuditsToInsert($doneAuditsData, $datetime) as $shop) {
                foreach ($shop as $audit) {
                    $this->doneAuditsRepository->insertDoneAudit(
                        $audit['date'],
                        $audit['salesRepresentative'],
                        $audit['signer'],
                        $audit['shouldBeSku'],
                        $audit['currentSku'],
                        $audit['id'],
                        $doctrine
                    );
                }
            }
            return $this->render('shop/upload/success.html.twig', [
                'message' => 'Zaktualizowano bazę wykonanych audytów!'
            ]); 
        }

        return $this->renderForm('shop/upload/doneaudits.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/upload/shoplist", name="upload_shop_list")
     */
    public function uploadShopList(Request $request, FileUploader $fileUploader, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(ShopListFileType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $doneAuditsFile */
            $shopListFile = $form->get('shopList')->getData();
            if ($shopListFile) {
                $filename = $fileUploader->upload($shopListFile);
            }
        }
        if (isset($filename)) {
            $spreadsheet = $this->xlsxFileLoader->loadFromFile($filename);
            $shopList = $this->shopListPreperer->getShopDatabase($spreadsheet);
            foreach ($shopList as $shop) {
                foreach ($this->categoryRepository->fetchAllCategories() as $category) {
                    if ($shop['category'] == $category->getCategoryName()) {
                        $this->shopRepository->insertShop(
                            $shop['regionalCompany'],
                            $shop['shortName'],
                            $shop['name'],
                            $shop['nip'],
                            $shop['postcode'],
                            $shop['city'],
                            $shop['adress'],
                            $category,
                            $shop['accessionDate'],
                            $doctrine
                        );
                    }
                }
            }    

            return $this->render('shop/upload/success.html.twig', [
                'message' => 'Zaktualizowano bazę sklepów!'
            ]); 
        }

        return $this->renderForm('shop/upload/shoplist.html.twig', [
            'form' => $form,
        ]);
    }


}