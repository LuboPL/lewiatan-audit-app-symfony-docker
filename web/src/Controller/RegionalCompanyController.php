<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\ShopRepository;
use App\Repository\DoneAuditsRepository;
use App\Model\AuditCalculator;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use App\Service\UriMapper;

class RegionalCompanyController extends AbstractController
{
    private ShopRepository $shopRepository;
    private DoneAuditsRepository $doneAuditsRepository;
    private AuditCalculator $auditCalculator;
    private RequestStack $requestStack;
    private UriMapper $uriMapper;


    public function __construct(ShopRepository $shopRepository, DoneAuditsRepository $doneAuditsRepository, AuditCalculator $auditCalculator, RequestStack $requestStack, UriMapper $uriMapper)
    {
        $this->shopRepository = $shopRepository;
        $this->auditCalculator = $auditCalculator;
        $this->requestStack = $requestStack;
        $this->doneAuditsRepository = $doneAuditsRepository;
        $this->uriMapper = $uriMapper;
    }
    
    #[Route('/shop/{regionalCompany}', name: 'shop_regional_company')]
    public function showShopsByRegionalCompany(Request $request, $regionalCompany, ManagerRegistry $doctrine): Response
    {   
        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);
        $shopsByCompany = $this->shopRepository->findByRegionalCompany($regionalCompanyQuery);
      
        return $this->render('shop/regionalCompany.html.twig', [
            'regionalCompany' => $regionalCompany,
            'shop' => $shopsByCompany,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }

    #[Route('/shop/{regionalCompany}/doneaudits', name: 'shop_regional_company_audits')]
    public function showDoneAuditsByRegionalCompany(Request $request, $regionalCompany, ManagerRegistry $doctrine): Response
    {   
        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);

        $audits = [];
        foreach ($this->doneAuditsRepository->fetchAllAudits() as $audit) {
            if ($audit->getShop()->getRegionalCompany() == $regionalCompanyQuery) {
                $audits[] = $audit;                
            }
        }

        return $this->render('shop/controlled/doneAudits.html.twig', [
            'regionalCompany' => $regionalCompany,
            'audits' => $audits,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }

    #[Route('/shop/{regionalCompany}/duplicatedaudits', name: 'shop_regional_company_duplicated')]
    public function showDuplicatedAuditsByRegionalCompany(Request $request, $regionalCompany, ManagerRegistry $doctrine): Response
    {   
        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);
        $audits = [];
        foreach ($this->auditCalculator->checkDuplicatedShop($regionalCompanyQuery) as $audit) {
            $audit->getShop()->getRegionalCompany();
            $audits[] = $audit;
        }
        
        return $this->render('shop/controlled/duplicatedAudits.html.twig', [
            'regionalCompany' => $regionalCompany,
            'audits' => $audits,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }

    #[Route('/shop/{regionalCompany}/calculated', name: 'shop_regional_company_calculated')]
    public function calculateRemainingShopsByRegionalCompany(Request $request, $regionalCompany, ManagerRegistry $doctrine): Response
    {   
        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);
        $calculatedShopsByCompany = $this->auditCalculator->calculateRemainingShops($regionalCompanyQuery);

        return $this->render('shop/controlled/calculatedRemainigAudits.html.twig', [
            'regionalCompany' => $regionalCompany,
            'shop' => $calculatedShopsByCompany,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }

    #[Route('/shop/{regionalCompany}/{category}', name: 'shop_category')]
    public function showRemainingShopsByCategory(Request $request, $regionalCompany, $category, ManagerRegistry $doctrine): Response
    {

        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);
        $categoryParam = $this->uriMapper->changeCategoryNameToId($category); 
        $remainingShops = $this->shopRepository->findRemainigShopsByCategory($regionalCompanyQuery, $categoryParam);

       
        return $this->render('shop/category.html.twig', [
            'shop' =>  $remainingShops,
            'regionalCompany' => $regionalCompany,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }

    #[Route('/shop/{regionalCompany}/random/{category}', name: 'shop_category_random')]
    public function randomRemainingShopsByCategory(Request $request, $regionalCompany, $category, ManagerRegistry $doctrine): Response
    {
        $regionalCompanyQuery = $this->uriMapper->changePathToDbQueryString($regionalCompany);
        $categoryParam = $this->uriMapper->changeCategoryNameToId($category); 
        $remainingShops = $this->auditCalculator->showRandomRemainigShopsToAuditByCategory($regionalCompanyQuery, $categoryParam);

        return $this->render('shop/categoryRandom.html.twig', [
            'shop' =>  $remainingShops,
            'regionalCompany' => $regionalCompany,
            'regionalCompanyQuery' => $regionalCompanyQuery
        ]);
    }
}
