<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Adapter\XlsxAdapter;
use App\Entity\DoneAudits;
use App\Entity\ControlledShops;
use App\Entity\ShopsToControll;
use App\Entity\Shop;
use Doctrine\Persistence\ManagerRegistry;

use App\Model\AuditCalculator;

class XlsxReaderController extends AbstractController
{

    #[Route('/xlsx/upload/shopstocontroll', name: 'xlsx_upload')]
    public function uploadShopsToControllData(ManagerRegistry $doctrine): Response
    {
        $shopsToControllSum = $this->xlsxAdapter->getShopsToControllSum();
        $entityManager = $doctrine->getManager();
        foreach ($shopsToControllSum as $audit) {
            $shopsToControll = new ShopsToControll();
            $shopsToControll->setRegionalCompany($audit['regionalCompany']);
            $shopsToControll->setCategoryA($audit['a']);
            $shopsToControll->setCategoryB($audit['b']);
            $shopsToControll->setCategoryC($audit['c']);
            $shopsToControll->setCategoryD($audit['d']);
            $shopsToControll->setCategoryE($audit['e']);
        
  
            $entityManager->persist($shopsToControll);
            $entityManager->flush();
         }

        return $this->render('xlsx_reader/index.html.twig', [
            'controller_name' => 'XlsxReaderController',
        ]);
    }

    #[Route('/xlsx/upload/shopdatabase', name: 'xlsx_upload_database')]
    public function uploadShopDatabase(ManagerRegistry $doctrine): Response
    {
        $shopDatabase = $this->xlsxAdapter->rewriteShopDatabase();
        $entityManager = $doctrine->getManager();
        foreach ($shopDatabase as $shop) {
            $shopData = new Shop();
            $shopData->setRegionalCompany($shop['regionalCompany']);
            $shopData->setShortName($shop['shortName']);
            $shopData->setName($shop['name']);
            $shopData->setNip($shop['nip']);
            $shopData->setPostcode($shop['postcode']);
            $shopData->setCity($shop['city']);
            $shopData->setAdress($shop['adress']);
            $shopData->setCategory($shop['category']);
            $shopData->setAccessionDate($shop['accessionDate']);
  
            $entityManager->persist($shopData);
            $entityManager->flush();
         }
        return $this->render('xlsx_reader/index.html.twig', [
            'controller_name' => 'XlsxReaderController',
        ]);
    }

    #[Route('/xlsx/download/shopdatabase', name: 'xlsx_download_database')]
    public function downloadShopDatabase(): Response
    {
        $shopDatabase = $this->xlsxAdapter->rewriteShopDatabase();

        var_dump($shopDatabase);
        return $this->render('xlsx_reader/index.html.twig', [
            'controller_name' => 'XlsxReaderController',
        ]);
    }
}
