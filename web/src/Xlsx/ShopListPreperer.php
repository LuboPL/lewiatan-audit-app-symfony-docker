<?php

namespace App\Xlsx;

class ShopListPreperer
{
    
    public function getShopDatabase($spreadsheet): array
    {
        $shopDatabase = $spreadsheet->getActiveSheet(3)->removeRow(1)->toArray(null, true, true, true);

        return $this->rewriteShopDatabase($shopDatabase);
    }

    private function rewriteShopDatabase($shopDatabase): array
    {
        $rewrittenShopDatabase = [];
        foreach ($shopDatabase as $row) {
            $rewrittenShopDatabase[] = [
                'id' => $row['A'], 
                'regionalCompany' => $row['B'], 
                'shortName' => $row['C'], 
                'name' => $row['D'], 
                'nip' => $row['E'],
                'postcode' => $row['F'],
                'city' => $row['G'], 
                'adress' => $row['H'], 
                'category' => $row['I'], 
                'accessionDate' => $row['J']
            ];
        }
        
        return $rewrittenShopDatabase;  
    }  
}
