<?php

namespace App\Xlsx;

class XlsxFileLoader
{
    public function loadFromFile($filename): object
    {
        $inputFileName = '../public/upload/'.$filename;
/*         sprintf() */
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        return $spreadsheet;
    }
}
