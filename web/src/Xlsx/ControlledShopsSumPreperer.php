<?php

namespace App\Xlsx;

class ControlledShopsSumPreperer
{
    
    public function getControlledShopsSum($spreadsheet): array
    {
        $controlledShopsByCompany = $spreadsheet->getSheet(1)->removeRow(1,2)->toArray(null, true, true, true);
        $controlledShopsSum = [];
        foreach ($controlledShopsByCompany as $row) {
            if (!is_null($row['A'])) {
                $controlledShopsSum[] = [
                    'regionalCompany' => $row['A'], 
                    'a' => $row['P'],
                    'b' => $row['Q'],
                    'c' => $row['R'],
                    'd' => $row['S'],
                    'e' => $row['T'],
                    'f' => $row['U']
                ];
            }
        }

        return $controlledShopsSum;
    } 
}
