<?php

namespace App\Xlsx;

class DoneAuditsDataPreperer
{
    
    public function getDoneAuditsData($spreadsheet): array
    {
        $doneAuditsRaw = $spreadsheet->getSheet(0)->removeRow(1)->toArray(null, true, true, true);

        return $this->rewriteSheetData($doneAuditsRaw);
    }

    private function rewriteSheetData($doneAuditsRaw): array
    {
        $rewrittenSheetData = [];
        foreach ($doneAuditsRaw as $row) {
            $rewrittenSheetData[] = [
                'regionalCompany' => $row['A'], 
                'name' => $row['B'], 
                'city' => $row['C'], 
                'adress' => $row['D'], 
                'nip' => $row['E'],
                'codeId' => $row['F'],
                'codeEleader' => $row['G'], 
                'date' => $row['H'], 
                'salesRepresentative' => $row['I'], 
                'signer' => $row['J'], 
                'category' => $row['K'],
                'shouldBeSku' => $row['L'],
                'currentSku' => $row['M']
            ];
        }

        return $rewrittenSheetData;  
    }
}
