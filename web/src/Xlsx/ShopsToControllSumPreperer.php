<?php

namespace App\Xlsx;

class ShopsToControllSumPreperer
{
    
    public function getShopsToControllSum($spreadsheet): array
    {
        $shopsToControll = $spreadsheet->getSheet(1)->removeRow(1,2)->toArray(null, true, true, true);
        $shopsToControllSum = [];
        foreach ($shopsToControll as $row) {
            if (!is_null($row['A'])) {
                $shopsToControllSum[] = [
                    'regionalCompany' => $row['A'], 
                    'a' => $row['I'],
                    'b' => $row['J'],
                    'c' => $row['K'],
                    'd' => $row['L'],
                    'e' => $row['M']
                ];
            }
        }

        return $shopsToControllSum;
    }
}
