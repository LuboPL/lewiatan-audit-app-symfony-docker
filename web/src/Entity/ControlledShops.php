<?php

namespace App\Entity;

use App\Repository\ControlledShopsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ControlledShopsRepository::class)]
class ControlledShops
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $a;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $b;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $c;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $d;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $e;

    #[ORM\Column(type: 'string', length: 121, nullable: true)]
    private $regionalCompany;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $f;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryA(): ?int
    {
        return $this->a;
    }

    public function setCategoryA(?int $a): self
    {
        $this->a = $a;

        return $this;
    }

    public function getCategoryB(): ?int
    {
        return $this->b;
    }

    public function setCategoryB(?int $b): self
    {
        $this->b = $b;

        return $this;
    }

    public function getCategoryC(): ?int
    {
        return $this->c;
    }

    public function setCategoryC(?int $c): self
    {
        $this->c = $c;

        return $this;
    }

    public function getCategoryD(): ?int
    {
        return $this->d;
    }

    public function setCategoryD(?int $d): self
    {
        $this->d = $d;

        return $this;
    }

    public function getCategoryE(): ?int
    {
        return $this->e;
    }

    public function setCategoryE(?int $e): self
    {
        $this->e = $e;

        return $this;
    }

    public function getRegionalCompany(): ?string
    {
        return $this->regionalCompany;
    }

    public function setRegionalCompany(?string $regionalCompany): self
    {
        $this->regionalCompany = $regionalCompany;

        return $this;
    }

    public function getCategoryF(): ?int
    {
        return $this->f;
    }

    public function setCategoryF(?int $f): self
    {
        $this->f = $f;

        return $this;
    }
}
