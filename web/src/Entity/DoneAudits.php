<?php

namespace App\Entity;

use App\Repository\DoneAuditsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DoneAuditsRepository::class)]
class DoneAudits
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', nullable: true)]
    private $date;

    #[ORM\Column(type: 'string', length: 121, nullable: true)]
    private $salesRepresentative;

    #[ORM\Column(type: 'string', length: 121, nullable: true)]
    private $signer;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $shouldBeSku;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $currentSku;

    #[ORM\ManyToOne(targetEntity: Shop::class, inversedBy: 'shop')]
    #[ORM\JoinColumn(nullable: false)]
    private Shop $shop;

    public function __construct(string $date, string $salesRepresentative, string $signer, ?int $shouldBeSku, ?int $currentSku, Shop $shop)
    {
        $this->date = $date;
        $this->salesRepresentative = $salesRepresentative;
        $this->signer = $signer;
        $this->shouldBeSku = $shouldBeSku;
        $this->currentSku = $currentSku;
        $this->shop = $shop;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSalesRepresentative(): ?string
    {
        return $this->salesRepresentative;
    }

    public function setSalesRepresentative(string $salesRepresentative): self
    {
        $this->salesRepresentative = $salesRepresentative;

        return $this;
    }

    public function getSigner(): ?string
    {
        return $this->signer;
    }

    public function setSigner(?string $signer): self
    {
        $this->signer = $signer;

        return $this;
    }

    public function getShouldBeSku(): ?int
    {
        return $this->shouldBeSku;
    }

    public function setShouldBeSku(?int $shouldBeSku): self
    {
        $this->shouldBeSku = $shouldBeSku;

        return $this;
    }

    public function getCurrentSku(): ?int
    {
        return $this->currentSku;
    }

    public function setCurrentSku(?int $currentSku): self
    {
        $this->currentSku = $currentSku;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

}
