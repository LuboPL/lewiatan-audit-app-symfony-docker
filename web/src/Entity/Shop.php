<?php

namespace App\Entity;

use App\Repository\ShopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity(repositoryClass: ShopRepository::class)]
class Shop
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 13, nullable: true)]
    private string $regional_company;

    #[ORM\Column(type: 'string', length: 27, nullable: true)]
    private string $short_name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private string $name;

    #[ORM\Column(type: 'bigint', nullable: true)]
    private int $nip;

    #[ORM\Column(type: 'string', length: 6, nullable: true)]
    private string $postcode;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private string $city;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    private string $adress;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'category')]
    #[ORM\JoinColumn(nullable: true)]
    private Category $category;

    #[ORM\Column(type: 'string', length: 121, nullable: true)]
    private string $accession_date;

    #[ORM\OneToMany(mappedBy: 'shop', targetEntity: DoneAudits::class)]
    private Collection $shop;

    public function __construct(string $regional_company, string $short_name, string $name, int $nip, string $postcode, string $city, string $adress, Category $category, string $accession_date)
    {
        $this->regional_company = $regional_company;
        $this->short_name = $short_name;
        $this->name = $name;
        $this->nip = $nip;
        $this->postcode = $postcode;
        $this->city = $city;
        $this->adress = $adress;
        $this->category = $category;
        $this->accession_date = $accession_date;
        $this->shop_id = new ArrayCollection();
        $this->shop = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegionalCompany(): ?string
    {
        return $this->regional_company;
    }

    public function setRegionalCompany(?string $regional_company): self
    {
        $this->regional_company = $regional_company;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->short_name;
    }

    public function setShortName(?string $short_name): self
    {
        $this->short_name = $short_name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNip(): ?int
    {
        return $this->nip;
    }

    public function setNip(?int $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getAccessionDate(): ?string
    {
        return $this->accession_date;
    }

    public function setAccessionDate(?string $accession_date): self
    {
        $this->accession_date = $accession_date;

        return $this;
    }

    /**
     * @return Collection|DoneAudits[]
     */
    public function getShop(): Collection
    {
        return $this->shop;
    }

    public function addShop(DoneAudits $shop): self
    {
        if (!$this->shop->contains($shop)) {
            $this->shop[] = $shop;
            $shop->setShop($this);
        }

        return $this;
    }

    public function removeShop(DoneAudits $shop): self
    {
        if ($this->shop->removeElement($shop)) {
            // set the owning side to null (unless already changed)
            if ($shop->getShop() === $this) {
                $shop->setShop(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
