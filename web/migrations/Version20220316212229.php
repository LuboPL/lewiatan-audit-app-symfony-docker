<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220316212229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE `admin` (
            `id` int NOT NULL,
            `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
            `roles` json NOT NULL,
            `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `category` (
            `id` int NOT NULL,
            `category_name` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `controlled_shops` (
            `id` int NOT NULL,
            `a` int DEFAULT NULL,
            `b` int DEFAULT NULL,
            `c` int DEFAULT NULL,
            `d` int DEFAULT NULL,
            `e` int DEFAULT NULL,
            `f` int DEFAULT NULL,
            `regional_company` varchar(121) COLLATE utf8mb4_unicode_ci DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `doctrine_migration_versions` (
            `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
            `executed_at` datetime DEFAULT NULL,
            `execution_time` int DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci');

        $this->addSql('CREATE TABLE `done_audits` (
            `id` int NOT NULL,
            `shop_id` int NOT NULL,
            `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `sales_representative` varchar(121) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `signer` varchar(121) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `should_be_sku` int DEFAULT NULL,
            `current_sku` int DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `messenger_messages` (
            `id` bigint NOT NULL,
            `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
            `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
            `queue_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
            `created_at` datetime NOT NULL,
            `available_at` datetime NOT NULL,
            `delivered_at` datetime DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `shop` (
            `id` int NOT NULL,
            `regional_company` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `short_name` varchar(27) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `nip` bigint DEFAULT NULL,
            `postcode` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `adress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `accession_date` varchar(121) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
            `category_id` int DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('CREATE TABLE `shops_to_controll` (
            `id` int NOT NULL,
            `a` int DEFAULT NULL,
            `b` int DEFAULT NULL,
            `c` int DEFAULT NULL,
            `d` int DEFAULT NULL,
            `e` int DEFAULT NULL,
            `f` int DEFAULT NULL,
            `regional_company` varchar(121) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

        $this->addSql('ALTER TABLE `admin`
            ADD PRIMARY KEY (`id`),
            ADD UNIQUE KEY `UNIQ_880E0D76E7927C74` (`email`)');

        $this->addSql('ALTER TABLE `category`
            ADD PRIMARY KEY (`id`)');   

        $this->addSql('ALTER TABLE `controlled_shops`
            ADD PRIMARY KEY (`id`)'); 

        $this->addSql('ALTER TABLE `doctrine_migration_versions`
            ADD PRIMARY KEY (`version`)'); 
            
        $this->addSql('ALTER TABLE `done_audits`
            ADD PRIMARY KEY (`id`),
            ADD KEY `IDX_F1D6D9D34D16C4DD` (`shop_id`)');
            
        $this->addSql('ALTER TABLE `messenger_messages`
            ADD PRIMARY KEY (`id`),
            ADD KEY `IDX_75EA56E016BA31DB` (`delivered_at`)');
            
        $this->addSql('ALTER TABLE `shop`
            ADD PRIMARY KEY (`id`),
            ADD KEY `IDX_AC6A4CA212469DE2` (`category_id`)'); 
            
        $this->addSql('ALTER TABLE `shops_to_controll`
            ADD PRIMARY KEY (`id`)');
            
        $this->addSql('ALTER TABLE `admin`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');
            
        $this->addSql('ALTER TABLE `category`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');
            
        $this->addSql('ALTER TABLE `controlled_shops`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');

        $this->addSql('ALTER TABLE `done_audits`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');

        $this->addSql('ALTER TABLE `messenger_messages`
            MODIFY `id` bigint NOT NULL AUTO_INCREMENT');
            
        $this->addSql('ALTER TABLE `shop`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');
            
        $this->addSql('ALTER TABLE `shops_to_controll`
            MODIFY `id` int NOT NULL AUTO_INCREMENT');

        $this->addSql('ALTER TABLE `done_audits`
            ADD CONSTRAINT `FK_F1D6D9D34D16C4DD` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`)');  
            
        $this->addSql('ALTER TABLE `shop`
            ADD CONSTRAINT `FK_AC6A4CA212469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)');          
    }

    public function down(Schema $schema): void
    {

    }
}
